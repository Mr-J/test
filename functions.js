const PATHS = {
    home: {
        path: "/",
        template: `
            <home-component></home-component>
            `,
    },
    about: {
        path: "/about",
        template: `
            <about-component></about-component>
        `,
    },
    contact: {
        path: "/contact",
        template: `
            <contact-component></contact-component>

            `,
    },
    gallery: {
        path: "/gallery",
        template: `
            <gallery-component></gallery-component>
        `,
    },
    services: {
        path: "/services",
        template: `
            <services-component></services-component>
        `,
    }

}
const ROUTER = new Router(PATHS);