class Header extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML = `
        <style>

       
        .header-main{
            opacity: 0.5 !important;
        }
    
        .phone-head {
            color: aliceblue;
            display: flex;
            justify-content: center;
        }
 

        .logo-many{
            width: 27%;
            cursor: pointer
        }

        .fa-phone-square{
            font-size: 22px
        }


        .nav{

            display: flex;
            justify-content: center;
            background: #D7DFDE;
            z-index: 100;
            opacity: 0.5;
        }

        .nav-link{
            cursor: pointer
        }


        </style>


        <header class"container">

            <nav class="navbar navbar-dark bg-dark row">
                <div class="phone-head col-5">
                    <i class="fas fa-phone-square "></i>
                    <p class="cel-phone ml-2" >(737) 356 1200</p>   
                </div>

                <div class="logo-container w-25 col-7">
                    <img onclick="ROUTER.load('home')" class="logo-many w-25" src="./assets/images/LOGO-many.png" alt="logo">
                </div>
                </div>
                
                
                
                </nav>      
                
                <div class="nav ">
                <a class="nav-link" id="about" onclick="ROUTER.load('about')">ABOUT US</a>
                <a class="nav-link" id="services" onclick="ROUTER.load('services')">SERVICES</a>
                <a class="nav-link" id="gallery" onclick="ROUTER.load('gallery')">GALLERY</a>
                <a class="nav-link" id="contact" onclick="ROUTER.load('contact')">CONTACT US</a>
                </div>
                
      
           

        </header>

   
      `;
    }
}

customElements.define('header-component', Header);