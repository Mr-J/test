class Footer extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML = `
        <style>

            .section-top-footer{
                background-color: #317BAC;
                padding: 4% 8%;
            }

            .section-button-footer{
                background-color: #0864A1;
                padding: 14px 0;
            }

            .logo-button-footer{
                width: 13%;
            }

            .footer-p, .footer-h5, .footer-h6{
                color: #FFFFFF
            }

            .icon{
                border-radius: 100%;
                background-color: #ffffff;
                font-size: 30px;
                padding: 6px;
            }

            .icon-strong{
                color: #04187E;
            }

            .email-strong{
                color: #8F9DE5;
            }
                
        </style>
                


        <footer>
            <div class="section-top-footer row">
                <div class="col-4">
                    <h5 class="footer-h5">Manny´s Services & Design</h5>
                    <p class="footer-p">2004 San Jose St Georgetown</p>
                    <p class="footer-p">Texas 78626</p>
                    <p class="footer-p"><strong class="icon-strong">Phone:</strong> (737) 356-1200</p>
                    <p class="footer-p"><strong class="icon-strong">Email:</strong> info@mannyservicedesign.com</p>
                    <div class="icons-social-media">
                        <a href="#"><i class="icon fab fa-facebook"></i></a>
                        <a href="#"><i class="icon fab fa-twitter"></i></a>
                        <a href="#"><i class="icon fab fa-instagram"></i></a>
                        <a href="#"><i class="icon fab fa-linkedin"></i></a>
                    </div>
                </div>
                <div class="col-4">
                    <h6 class="footer-h6">Menu</h6>
                    <p class="footer-p"><strong>></strong>About us</p>
                    <p class="footer-p"><strong>></strong>Services</p>
                    <p class="footer-p"><strong>></strong>Gallery</p>
                    <p class="footer-p"><strong>></strong>Contact us</p>
                </div>
                <div class="col-4">
                    <h6 class="footer-h6">Services</h6>
                    <p class="footer-p"><strong>></strong>Title Repair</p>
                    <p class="footer-p"><strong>></strong>Drywall Repair</p>
                    <p class="footer-p"><strong>></strong>Painting</p>
                    <p class="footer-p"><strong>></strong>Plumbing</p>
                </div>

            </div>


            <div class="section-button-footer row">
                <div class="col-12 text-center">
                    <p class="footer-p">© Copyright <strong class="icon-strong">Manny´s Services & Design</strong>. All Rights Reserved</p>
                    <p class="footer-p">This Website Was Designed And Developed by <strong class="email-strong">Josesilvera.com</strong></p>
                    <img class="logo-button-footer" src="./assets//images/Logo-JS-White-Wshadow.png" alt="logo-joseSilvera">
                </div>

            </div>


        </footer>
        `;
    }
}


customElements.define('footer-component', Footer);