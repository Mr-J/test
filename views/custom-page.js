class CustomPage extends HTMLElement {


    constructor(html, pageCss) {
        super();
        this.html = html;
        this.pageCss = pageCss;
    }

    connectedCallback() {
        const xhttp = new XMLHttpRequest();
        xhttp.open('GET', this.html, false);
        xhttp.send();
        const page = xhttp.responseText;

        xhttp.open('GET', this.pageCss, false);
        xhttp.send();
        const pageCss = `<style>${xhttp.responseText}</style>`;

        this.innerHTML = pageCss + page;
    }



}