class About extends CustomPage {
    constructor() {
        super('views/about/about.html', 'views/about/about.css');
    }

}

customElements.define('about-component', About);