class Gallery extends CustomPage {
    constructor() {
        super('views/gallery/gallery.html', 'views/gallery/gallery.css');
    }

}

customElements.define('gallery-component', Gallery);