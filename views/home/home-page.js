class Home extends CustomPage {
    constructor() {
        super('views/home/home.html', 'views/home/style.css');
    }

}

customElements.define('home-component', Home);